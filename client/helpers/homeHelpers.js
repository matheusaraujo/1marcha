/**
 * Created by matheus on 8/1/15.
 */
Template.home.helpers({
    problems: function() {
        return CarProblems.find().fetch().map(function(it){ return it.problem; });
    }
});