Template.home.rendered = function() {
    $(document).ready(function(){
        $('ul.tabs').tabs();
        $('ul.tabs').tabs('select_tab', 'test1');

        $("input[name='carproblem']").click(function(){
            $('ul.tabs').tabs('select_tab', 'test1');
        });
        $("#addServices").click(function(){
            $('ul.tabs').tabs('select_tab', 'test1');
        });
        $("#results").click(function(){
            $('ul.tabs').tabs('select_tab', 'test2');
        });
        $(".addCard").click(function(){
            $(this).parent().parent().fadeOut();
        });


        var items = CarProblems.find().fetch().map(function(it){ return it.problem; });

        $('#search').keyup(function() {
            var search = $(this).val();

            /* Matching logic */
            var matches = items.filter(function(item) {
                item = ' ' + item
                var j = 0; // remembers position of last found character

                // consider each search character one at a time
                for (var i = 0; i < search.length; i++) {
                    var l = search[i];
                    if (l == ' ') continue;     // ignore spaces

                    j = item.indexOf(l, j+1);     // search for character & update position
                    if (j == -1) return false;  // if it's not found, exclude this item
                }
                return true;
            });
            /* End matching logic */

            $('#results').empty();
            matches.forEach(function(match) {
                $('#results').append($('<a href="#" class="collection-item orange lighten-5">').text(match));
            });
        }).keyup();
    });

};